var express = require('express');
var app = express();

app.get('/', function (req, res) {
  res.send('Hello WorldX!');
});

app.get('/demo', function (req, res) {
    res.send('DemoX WebIde');
  });

app.listen(5000, function () {
  console.log('Example app listening on port 5000!');
});